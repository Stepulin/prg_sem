#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#define ARG_HELP "-h"
#define ARG_ITER "-n"
#define ARG_WIDTH "-wi"
#define ARG_HEIGHT "-hi"
#define ARG_DEVICE "-d"
#define ARG_C_RE "-cr"
#define ARG_C_IM "-ci"
#define ARG_RMIN "-rmin"
#define ARG_RMAX "-rmax"
#define ARG_IMIN "-imin"
#define ARG_IMAX "-imax"

void *keyboard_thread(void *d);
void print_help(void);

#endif
