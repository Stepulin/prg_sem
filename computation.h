#ifndef __COMPUTATION_H__
#define __COMPUTATION_H__

#include "messages.h"
#include <bits/stdint-uintn.h>
#include "main.h"
#include <stdbool.h>

typedef struct {
  double c_re;
  double c_im;
  int n;

  double range_re_min;
  double range_re_max;
  double range_im_min;
  double range_im_max;

  int grid_w;
  int grid_h;
  int cur_x;
  int cur_y;

  double d_re;
  double d_im;

  int nbr_chunks;
  int cid;
  double chunk_re;
  double chunk_im;

  uint8_t chunk_n_re;
  uint8_t chunk_n_im;
  uint8_t *grid;
  bool computing;
  bool set;
  bool abort;
  bool done;
  bool next_comp;
} Computation;

void computation_init(init_data *data);
void computation_cleanup(void);
bool is_computing(void);
bool is_done(void);
void abort_comp(void);
bool is_abort(void);
bool is_set(void);
void enable_comp(void);
bool next_comp_available(void);

void get_grid_size(int *w, int *h);
bool set_compute(message *msg);

bool compute(message *msg);

void update_data(msg_compute_data *compute_data);
void update_image(int w, int h, uint8_t *img);
void clear_buffer(void);
void reset_chunk(void);
//void change_axes(float inc);

void compute_julia_set();
int is_in_julia_set(Computation comp, double z_re, double z_im, int iter);

#endif
