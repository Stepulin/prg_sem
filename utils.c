#include "utils.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

void my_assert(bool r, const char *fcname, int line, const char *fname) {
  if (!r) {
    fprintf(stderr, "ERROR: my_assert failed: %s() line %d in %s\n", fcname,
            line, fname);
    exit(EXIT_FAILURE);
  }
}

void *my_alloc(size_t size) {
  void *ret = malloc(size);
  if (!ret) {
    fprintf(stderr, "ERROR: Cannot allocate memory\n");
    exit(EXIT_FAILURE);
  }
  return ret;
}

void call_termios(int reset) {
  static struct termios tio, tioOld;
  tcgetattr(STDIN_FILENO, &tio);
  if (reset) {
    tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
  } else {
    tioOld = tio;
    cfmakeraw(&tio);
    tio.c_oflag |= OPOST;
    tcsetattr(STDIN_FILENO, TCSANOW, &tio);
  }
}

void info(const char *s) { fprintf(stderr, "INFO: %s\n", s); }
void debug(const char *s) { fprintf(stderr, "DEBUG: %s\n", s); }
void error(const char *s) { fprintf(stderr, "ERROR: %s\n", s); }
