#include "event_queue.h"
#include "keyboard.h"
#include "main.h"
#include "messages.h"
#include "prg_serial_nonblock.h"
#include "utils.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define SERIAL_READ_TIMEOUT_MS 100

void *read_pipe_thread(void *d);

int main(int argc, char **argv) {
  init_data init = {.c_im = 0.6,
                    .c_re = -0.4,
                    .r_min = -1.6,
                    .r_max = 1.6,
                    .i_min = -1.1,
                    .i_max = 1.1,
                    .n = 60,
                    .grid_w = 640,
                    .grid_h = 480,
                    .device = "/dev/ttyACM0"};

  for (int i = 1; i < argc; ++i) {
    if (strcmp(ARG_ITER, argv[i]) == 0) {
      init.n = atoi(argv[++i]);
    } else if (strcmp(ARG_HELP, argv[i]) == 0) {
      print_help();
    } else if (strcmp(ARG_WIDTH, argv[i]) == 0) {
      init.grid_w = atoi(argv[++i]);
    } else if (strcmp(ARG_HEIGHT, argv[i]) == 0) {
      init.grid_h = atoi(argv[++i]);
    } else if (strcmp(ARG_DEVICE, argv[i]) == 0) {
      init.device = argv[++i];
    } else if (strcmp(ARG_C_RE, argv[i]) == 0) {
      init.c_re = atof(argv[++i]);
    } else if (strcmp(ARG_C_IM, argv[i]) == 0) {
      init.c_im = atof(argv[++i]);
    } else if (strcmp(ARG_RMIN, argv[i]) == 0) {
      init.r_min = atof(argv[++i]);
    } else if (strcmp(ARG_RMAX, argv[i]) == 0) {
      init.r_max = atof(argv[++i]);
    } else if (strcmp(ARG_IMIN, argv[i]) == 0) {
      init.i_min = atof(argv[++i]);
    } else if (strcmp(ARG_IMAX, argv[i]) == 0) {
      init.i_max = atof(argv[++i]);
    }
  }

  data_t data = {.quit = false, .fd = -1, .init = init};
  const char *serial = init.device;
  data.fd = serial_open(serial);

  my_assert(data.fd != -1, __func__, __LINE__, __FILE__);

  enum { KEYBOARD, SERIAL_READ, MAIN, NUM_THREADS };
  const char *thread_names[] = {"Keyboard", "Serial Rx", "Main"};
  void *(*thr_functions[])(void *) = {keyboard_thread, read_pipe_thread,
                                      main_thread};

  pthread_t threads[NUM_THREADS];
  call_termios(0);
  for (int i = 0; i < NUM_THREADS; ++i) {
    int r = pthread_create(&threads[i], NULL, thr_functions[i], &data);
    fprintf(stderr, "\rINFO: Create thread '%s' %s\r\n", thread_names[i],
            (r == 0 ? "OK" : "FAIL"));
  }

  for (int i = 0; i < NUM_THREADS; ++i) {
    fprintf(stderr, "INFO: Call join to the thread %s\r\n", thread_names[i]);
    int r = pthread_join(threads[i], NULL);
    fprintf(stderr, "INFO: Joining the thread %s hase been %s\r\n",
            thread_names[i], (r == 0 ? "OK" : "FAIL"));
  }
  serial_close(data.fd);
  call_termios(1);
  return EXIT_SUCCESS;
}

void *read_pipe_thread(void *d) {
  my_assert(d != NULL, __func__, __LINE__, __FILE__);
  data_t *data = (data_t *)d;
  fprintf(stderr, "read_pipe_thread start\r\n");
  bool end = false;
  u_int8_t msg_buf[sizeof(message)];

  unsigned char c;
  int i = 0;
  int len = 0;
  while (serial_getc_timeout(data->fd, SERIAL_READ_TIMEOUT_MS, &c) > 0) {
  }

  while (!end) {
    int r = serial_getc_timeout(data->fd, SERIAL_READ_TIMEOUT_MS, &c);
    if (r > 0) {
      if (i == 0) {
        if (get_message_size(c, &len)) {
          msg_buf[i++] = c;
        } else {
          fprintf(stderr, "ERROR: Unknown message type\n");
        }
      } else { // remainging bytes
        msg_buf[i++] = c;
      }
      if (len > 0 && i == len) {
        message *msg = my_alloc(sizeof(message));
        if (parse_message_buf(msg_buf, len, msg)) {
          event ev = {.type = EV_SERIAL_READ, .data.msg = msg};
          queue_push(ev);
        } else {
          fprintf(stderr, "ERROR: Cannot parse message\n");
          free(msg);
        }
        i = len = 0;
      }

    } else if (r == 0) {
    } else { // error
      fprintf(stderr, "ERROR: Reading from serial\n");
      set_quit();
      event ev = {.type = EV_QUIT};
      queue_push(ev);
    }
    end = is_quit();
  }

  fprintf(stderr, "read_pipe_thread finish\r\n");
  return NULL;
}
