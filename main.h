#ifndef __MAIN_H__
#define __MAIN_H__
#include <stdbool.h>

typedef struct {
  double c_re;
  double c_im;
  int grid_w;
  int grid_h;
  int n;
  double r_min;
  double r_max;
  double i_min;
  double i_max;
  char *device;
} init_data;

typedef struct {
  bool quit;
  int fd;
  init_data init;
}data_t;


void *main_thread(void *d);
void show_help(void);

#endif
