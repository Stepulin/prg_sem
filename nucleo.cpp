#include "mbed.h"
#include "messages.h"
#include <cmath>

/*-------MACROS-------*/
#define MSG_SIZE (sizeof(message))
#define BUF_SIZE 255
#define PERIOD 0.2
#define COMPUTE_TIME 0.3
#define NUM_BLINK 5
#define BAUD_RATE 115200
#define VERSION_MAJOR 3
#define VERSION_MINOR 8
#define VERSION_PATCH 5

/*---------GLOBAL VARIABLES-------*/
Serial serial(SERIAL_TX, SERIAL_RX);

InterruptIn button_event(USER_BUTTON);
Ticker ticker;
DigitalOut myled(LED1);

volatile bool computing = false;
volatile bool abort_request = false;
char tx_buffer[BUF_SIZE];
char rx_buffer[BUF_SIZE];
 
// pointers to the circular buffers
volatile int tx_in = 0;
volatile int tx_out = 0;
volatile int rx_in = 0;
volatile int rx_out = 0;

/*-------FUNCTION PROTOTYPES--------*/ 
void Tx_interrupt();
void Rx_interrupt();
bool send_buffer(const uint8_t* msg, unsigned int size);
bool receive_message(uint8_t* msg_buf, int size, int* len);
bool send_message(const message* msg, uint8_t* buf, int size);
uint8_t compute_iteration(double c_re, double c_im, int max_iter, double re, double im);
void tick();
void button();

struct compute_data{
    double c_re;
    double c_im;
    double d_re;
    double d_im;
    uint8_t n;
    uint8_t cid;
    double re;
    double im;
    uint8_t n_re;
    uint8_t n_im;
}compute_data;

int main(void){
    float period = PERIOD;
    float compute_time = COMPUTE_TIME;
    message msg = {.type = MSG_STARTUP, .data.startup.message = {'P','R','G','-','H','W',' ','1','0'}};
    serial.baud(BAUD_RATE);
    uint8_t msg_buf[MSG_SIZE];
    int msg_len;
    
    serial.attach(&Rx_interrupt, Serial::RxIrq); // attach interrupt handler to receive data
    serial.attach(&Tx_interrupt, Serial::TxIrq); // attach interrupt handler to transmit data
    button_event.rise(&button);
    
    /* init blinking*/
    for(int i = 0; i < NUM_BLINK * 2; ++i){
        myled = !myled;
        wait(PERIOD);
    }
    send_message(&msg, msg_buf, MSG_SIZE);
    while (1) {
        if (abort_request) {
            if (computing) {  //abort computing
                msg.type = MSG_ABORT;
                send_message(&msg, msg_buf, MSG_SIZE);
                computing = false;
                abort_request = false;
                ticker.detach();
                myled = 0;
            }
        }
        if (rx_in != rx_out) { // something is in the receive buffer
            if (receive_message(msg_buf, MSG_SIZE, &msg_len)) {
                if (parse_message_buf(msg_buf, msg_len, &msg)) {
                    switch(msg.type) {
                        case MSG_GET_VERSION:
                            msg.type = MSG_VERSION;
                            msg.data.version.major = VERSION_MAJOR;
                            msg.data.version.minor = VERSION_MINOR;
                            msg.data.version.patch = VERSION_PATCH;
                            send_message(&msg, msg_buf, MSG_SIZE);
                            break;
                        case MSG_ABORT:
                            msg.type = MSG_OK;
                            send_message(&msg, msg_buf, MSG_SIZE);
                            computing = false;
                            abort_request = false;
                            ticker.detach();
                            myled = 0;
                            break;
                        case MSG_SET_COMPUTE:
                            compute_data.c_re = msg.data.set_compute.c_re;
                            compute_data.c_im = msg.data.set_compute.c_im;
                            compute_data.d_re = msg.data.set_compute.d_re;
                            compute_data.d_im = msg.data.set_compute.d_im;
                            msg.type = MSG_OK;
                            send_message(&msg,msg_buf,MSG_SIZE);
                            break;
                        case MSG_COMPUTE:
                            ticker.attach(tick, period);
                            compute_data.cid = msg.data.compute.cid;
                            compute_data.re = msg.data.compute.re; //origin of chunk (re)
                            compute_data.im = msg.data.compute.im; //origin of chunk (im)
                            compute_data.n_re = msg.data.compute.n_re; //pixels in x coord of one chunk
                            compute_data.n_im = msg.data.compute.n_im; //pixels in y coord of one chunk
                            computing = true;
                            msg.type = MSG_OK;
                            send_message(&msg, msg_buf, MSG_SIZE);
                            break;
                        case MSG_CHANGE_BAUD_RATE:
                        serial.baud(691200);
                            wait_ms(5);
                            msg.type = MSG_OK;
                            send_message(&msg,msg_buf,MSG_SIZE);
                            
                            break;
                    } // end switch
                } else { // message has not been parsed send error
                    msg.type = MSG_ERROR;
                    send_message(&msg, msg_buf, MSG_SIZE);
                }
            } // end message received
        } else if (computing) {
            msg.type = MSG_COMPUTE_DATA;
            uint8_t cid = compute_data.cid;
            // for every pixel in chunk
            for(int y = 0; y < compute_data.n_im; ++y){
                for(int x = 0; x < compute_data.n_re; ++x){
                    // compute complex nuber = origin + pixel * infinitesimal piece
                    double t_re = compute_data.re + (x * compute_data.d_re);
                    double t_im = compute_data.im + (y * compute_data.d_im);
                    
                    msg.data.compute_data.iter = compute_iteration(compute_data.c_re, compute_data.c_im, compute_data.n, t_re, t_im);
                    msg.data.compute_data.cid = compute_data.cid;
                    msg.data.compute_data.i_re = x;
                    msg.data.compute_data.i_im = y;
                    send_message(&msg,msg_buf,MSG_SIZE);
                }
            }
            //chunk completed
            compute_data.cid = ++cid;
            ticker.detach();
            myled = 0;
            msg.type = MSG_DONE;
            send_message(&msg,msg_buf,MSG_SIZE);
            computing = false;
        } else {
            sleep(); // put the cpu to sleep mode, it will be wakeup on interrupt
        }
    } // end while (1)
}

uint8_t compute_iteration(double c_re, double c_im, int max_iter, double re, double im){
    uint8_t n = 1;
    double old_re = (re * re - im * im) + c_re;
    double old_im = (2 * re * im) + c_im;
    double new_re = old_re;
    double new_im = old_im;
    while(sqrt(new_re * new_re + new_im * new_im) < 2.0 && n < max_iter){
        old_re = new_re;
        old_im = new_im;
        new_re = (old_re * old_re - old_im * old_im) + c_re;
        new_im = (2 * old_re * old_im) + c_im;
        n+=1;
    }
    return n;
}
    
void tick(){
    myled = !myled;
}

void button(){
   abort_request = true;
}

bool send_message(const message *msg, uint8_t *buf, int size)
{
    return fill_message_buf(msg, buf, MSG_SIZE, &size) && send_buffer(buf, size);
}

bool send_buffer(const uint8_t* msg, unsigned int size){
    if (!msg && size == 0) {
        return false;    // size must be > 0
    }
    int i = 0;
    NVIC_DisableIRQ(USART2_IRQn); // start critical section for accessing global data
    USART2->CR1 |= USART_CR1_TXEIE; // enable Tx interrupt on empty out buffer
    bool empty = (tx_in == tx_out);
    while ( (i == 0) || i < size ) { //end reading when message has been read
        if ( ((tx_in + 1) % BUF_SIZE) == tx_out) { // needs buffer space
            NVIC_EnableIRQ(USART2_IRQn); // enable interrupts for sending buffer
            while (((tx_in + 1) % BUF_SIZE) == tx_out) {
                /// let interrupt routine empty the buffer
            }
            NVIC_DisableIRQ(USART2_IRQn); // disable interrupts for accessing global buffer
        }
        tx_buffer[tx_in] = msg[i];
        i += 1;
        tx_in = (tx_in + 1) % BUF_SIZE;
    } // send buffer has been put to tx buffer, enable Tx interrupt for sending it out
    USART2->CR1 |= USART_CR1_TXEIE; // enable Tx interrupt
    NVIC_EnableIRQ(USART2_IRQn); // end critical section
    return true;
}

bool receive_message(uint8_t* msg_buf, int size, int* len){
    bool ret = false;
    int i = 0;
    *len = 0; // message size
    NVIC_DisableIRQ(USART2_IRQn); // start critical section for accessing global data
    while ( ((i == 0) || (i != *len)) && i < size ) {
        if (rx_in == rx_out) { // wait if buffer is empty
            NVIC_EnableIRQ(USART2_IRQn); // enable interrupts for receing buffer
            while (rx_in == rx_out) { // wait of next character
            }
            NVIC_DisableIRQ(USART2_IRQn); // disable interrupts for accessing global buffer
        }
        uint8_t c = rx_buffer[rx_out];
        if (i == 0) { // message type
            if (get_message_size(c, len)) { // message type recognized
                msg_buf[i++] = c;
                ret = *len <= size; // msg_buffer must be large enough
            } else {
                ret = false;
                break; // unknown message
            }
        } else {
            msg_buf[i++] = c;
        }
        rx_out = (rx_out + 1) % BUF_SIZE;
    }
    NVIC_EnableIRQ(USART2_IRQn); // end critical section
    return ret;
}

void Tx_interrupt(){
    // send a single byte as the interrupt is triggered on empty out buffer 
    if (tx_in != tx_out) {
        serial.putc(tx_buffer[tx_out]);
        tx_out = (tx_out + 1) % BUF_SIZE;
    } else { // buffer sent out, disable Tx interrupt
        USART2->CR1 &= ~USART_CR1_TXEIE; // disable Tx interrupt
    }
    return;
}

void Rx_interrupt(){
    // receive bytes and stop if rx_buffer is full
    while ((serial.readable()) && (((rx_in + 1) % BUF_SIZE) != rx_out)) {
        rx_buffer[rx_in] = serial.getc();
        rx_in = (rx_in + 1) % BUF_SIZE;
    }
    return;
}


