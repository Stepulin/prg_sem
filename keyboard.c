#include "keyboard.h"
#include "computation.h"
#include "event_queue.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

void *keyboard_thread(void *d) {
  fprintf(stderr, "keyboard_thread start\r\n");
  int c;
  event ev;
  while ((c = getchar()) != 'q') {
    ev.type = EV_TYPE_NUM;
    switch (c) {

    case 'q':
      break;

    case 'g': // get version
      ev.type = EV_GET_VERSION;
      break;

    case 's': // set compute
      ev.type = EV_SET_COMPUTE;
      break;

    case 'h':
      ev.type = EV_SHOW_HELP;
      break;

    case 'l':
      ev.type = EV_CLEAR_BUFFER;
      break;

    case 'r':
      ev.type = EV_RESET_CHUNK;
      break;

    case 'b':
      ev.type = EV_CHANGE_BAUD_RATE;
      break;

    case '1': // compute
      if (next_comp_available()) {
        ev.type = EV_COMPUTE;
      } else {
        fprintf(stderr, "WARN: New computation requested but it is discarded "
                        "due on ongoing computation\n");
      }
      break;

    case 'c':
      ev.type = EV_COMPUTE_CPU;
      break;

    case 'a': // abort
      ev.type = EV_ABORT;
      break;

    case 'e':
      ev.type = EV_EXPORT;
      break;

    default:
      break;
    }
    if (ev.type != EV_TYPE_NUM) {
      queue_push(ev);
    }
  }
  set_quit();
  ev.type = EV_QUIT;
  queue_push(ev);
  fprintf(stderr, "keyboard_thread finish\r\n");
  return NULL;
}

void print_help(void) {
  printf(
      "Usage: ./hw10-main [-h HELP] [-d DEVICE] [-wi IMAGE WIDTH (px)] [-hi "
      "IMAGE HEIGHT (px)] [-n NUMBER OF ITERATIONS] [-cr COMPLEX PARAMETER "
      "- REAL] [-ci COMPLEX PARAMETER - IMAGINARY] [-rmin MINIMUM VALUE ON "
      "REAL AXIS] [-rmax MAXIMUM VALUE ON REAL AXIS] [-imin MINIMUM VALUE ON "
      "THE IMAGINARY AXIS] [-imax MAXIMUM VALUE ON THE IMAGINARY AXIS]\n");
  printf("\nOptions:\n");
  printf("-h \t Shows help\n");
  printf("-d \t Sets device port - default /dev/ttyACM0\n");
  printf("-wi \t Sets image width - default 640px\n");
  printf("-hi \t Sets image height - default 480px\n");
  printf("-n \t Sets number of iterations - default 60\n");
  printf("-cr \t Sets real part of complex parameter - default -0.4\n");
  printf("-ci \t Sets imaginary part of complex parameter - default +0.6\n");
  printf("-rmin \t Sets minimum value on real axis - default -1.6\n");
  printf("-rmax \t Sets maximum value on real axis - default +1.6\n");
  printf("-imin \t Sets minimum value on imaginary axis - default -1.1\n");
  printf("-imax \t Sets maximum value on imaginary axis - default +1.1\n");
  exit(EXIT_SUCCESS);
}
