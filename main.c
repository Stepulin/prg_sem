#include "main.h"
#include "computation.h"
#include "event_queue.h"
#include "gui.h"
#include "messages.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

void process_read_message(event *ev);

void *main_thread(void *d) {
  my_assert(d != NULL, __func__, __LINE__, __FILE__);
  data_t *data = (data_t *)d;
  message msg;
  u_int8_t msg_buf[sizeof(message)];
  int msg_len;
  bool quit = false;

  queue_init();
  computation_init(&(data->init));
  gui_init();
  do {
    event ev = queue_pop();
    msg.type = MSG_NBR;
    switch (ev.type) {

    case EV_QUIT:
      debug("Quit recieved");
      break;

    case EV_CHANGE_BAUD_RATE:
      if (is_computing()) {
        fprintf(stderr, "WARN: Cannot change baud rate while computing\n");
      } else {
        fprintf(stderr, "INFO: Baud rate changed to 691200 bps\n");
        msg.type = MSG_CHANGE_BAUD_RATE;
      }
      break;

    case EV_RESET_CHUNK:
      if ((is_abort() && is_computing()) || !is_computing()) {
        fprintf(stderr, "INFO: Chunk reset request\n");
        reset_chunk();
      } else {
        fprintf(stderr, "WARN: Chunk reset request discarded\n");
      }
      break;

    case EV_EXPORT:
      info("Export image to png");
      gui_export();
      break;

    case EV_SHOW_HELP:
      show_help();
      break;

    case EV_CLEAR_BUFFER:
      // clear buffer
      clear_buffer();
      gui_refresh();
      break;

    case EV_GET_VERSION:
      msg.type = MSG_GET_VERSION;
      fprintf(stderr, "INFO: Get version requested\n");
      break;

    case EV_SET_COMPUTE:
      info(set_compute(&msg) ? "set compute" : "fail set compute");
      break;

    case EV_COMPUTE:
      if (is_set()) {
        compute(&msg);
      } else {
        fprintf(stderr, "WARN: Parameters is not set\n");
      }
      break;

    case EV_ABORT:
      if (!is_computing()) {
        fprintf(stderr, "WARN: Abort requested but it is not computing\n");
      } else {
        msg.type = MSG_ABORT;
        abort_comp();
      }
      break;

    case EV_COMPUTE_CPU:
      if (is_set()) {
        compute_julia_set();
      } else {
        fprintf(stderr, "WARN: Parameters is not set\n");
      }
      break;

    case EV_SERIAL_READ:
      process_read_message(&ev);
      break;

    default:
      break;
    }
    if (msg.type != MSG_NBR) {
      my_assert(fill_message_buf(&msg, msg_buf, sizeof(message), &msg_len),
                __func__, __LINE__, __FILE__);
      if (write(data->fd, msg_buf, msg_len) == msg_len) {
        debug("Send data done");
      } else {
        error("Send data failed");
      }
    }

    quit = is_quit();
  } while (!quit);
  gui_cleanup();
  computation_cleanup();
  queue_cleanup();
  return NULL;
}

void process_read_message(event *ev) {
  my_assert(ev != NULL && ev->type == EV_SERIAL_READ && ev->data.msg, __func__,
            __LINE__, __FILE__);
  ev->type = EV_TYPE_NUM;
  message *msg = ev->data.msg;
  switch (msg->type) {

  case MSG_OK:
    fprintf(stderr, "INFO: Receive ok from Nucleo\n");
    break;

  case MSG_VERSION:
    fprintf(stderr, "INFO: Module version %d.%d-p%d", msg->data.version.major,
            msg->data.version.minor, msg->data.version.patch);
    break;

  case MSG_DONE:
    gui_refresh();
    if (is_done()) {
      fprintf(stderr,
              "INFO: Nucleo reports the computation is done computing: %d\n",
              is_done());
      event ev = {.type = EV_COMPUTE};
      queue_push(ev);

    } else {
    }
    break;

  case MSG_COMPUTE_DATA:
    if (!is_abort()) {
      update_data(&(msg->data.compute_data));
    }
    break;

  case MSG_STARTUP:
    fprintf(stderr, "INFO: Nucleo restarted - '%s'\n",
            msg->data.startup.message);
    break;

  case MSG_ABORT:
    if (!is_computing()) {
    } else {
      fprintf(stderr, "INFO: Abort from Nucleo\n");
      abort_comp();
    }
    break;

  case MSG_ERROR:
    fprintf(stderr, "WARN: Receive error from Nucleo\n");
    break;

  default:
    fprintf(stderr, "Unhandled message type %d\n", msg->type);
    break;
  }
  free(ev->data.msg);
  ev->data.msg = NULL;
}

void show_help() {
  printf("How to use this program:\n");
  printf("Key 's':\t Prepares parameters for computation\n");
  printf("Key '1':\t Starts communication with Nucleo\n");
  printf("Key 'g':\t Prints actual version of firmware\n");
  printf("Key 'a':\t Aborts computation\n");
  printf("Key 'r':\t Resets chunk id\n");
  printf("Key 'e':\t Exports image to png\n");
  printf("Key 'l':\t Deletes all data in buffer\n");
  printf("Key 'p':\t Redraw all data in buffer\n");
  printf("Key 'c':\t Computes fractal on PC\n");
  printf("Key 'q':\t Quits program\n");
}
