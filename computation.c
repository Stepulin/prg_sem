#include "computation.h"
#include "gui.h"
#include "main.h"
#include "messages.h"
#include "utils.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

Computation comp = {.grid = NULL,
                    .computing = false,
                    .abort = false,
                    .set = false,
                    .done = true};

void computation_init(init_data *data) {
  my_assert(data != NULL, __func__, __LINE__, __FILE__);
  comp.c_re = data->c_re;
  comp.c_im = data->c_im;
  comp.grid_w = data->grid_w;
  comp.grid_h = data->grid_h;
  comp.n = data->n;
  comp.range_re_min = data->r_min;
  comp.range_re_max = data->r_max;
  comp.range_im_min = data->i_min;
  comp.range_im_max = data->i_max;
  comp.chunk_n_re = comp.grid_w / 10;
  comp.chunk_n_im = comp.grid_h / 10;
  comp.next_comp = true; // unlock

  comp.grid = my_alloc(comp.grid_w * comp.grid_h);
  comp.d_re = (comp.range_re_max - comp.range_re_min) / (1. * comp.grid_w);
  comp.d_im = -(comp.range_im_max - comp.range_im_min) / (1. * comp.grid_h);
  comp.nbr_chunks =
      (comp.grid_w * comp.grid_h) / (comp.chunk_n_re * comp.chunk_n_im);
}

void computation_cleanup(void) {
  if (comp.grid) {
    free(comp.grid);
  }
  comp.grid = NULL;
}

bool is_computing(void) { return comp.computing; }
bool is_done(void) { return comp.done; }
void abort_comp(void) {
  comp.next_comp = true;
  comp.abort = true;
}
bool is_abort(void) { return comp.abort; }
bool is_set(void) { return comp.set; }
void enable_comp(void) {
  comp.next_comp = false;
  comp.abort = false;
}
bool next_comp_available(void) { return comp.next_comp; }
void reset_chunk() { comp.computing = false; }

bool set_compute(message *msg) {
  my_assert(msg != NULL, __func__, __LINE__, __FILE__);
  bool ret = !is_computing();
  if (ret) {
    fprintf(stderr,
            "INFO: Set new computation resolution %dx%d no. of chunks: %d\n",
            comp.grid_w, comp.grid_h, comp.nbr_chunks);
    msg->type = MSG_SET_COMPUTE;
    msg->data.set_compute.c_re = comp.c_re;
    msg->data.set_compute.c_im = comp.c_im;
    msg->data.set_compute.d_re = comp.d_re;
    msg->data.set_compute.d_im = comp.d_im;
    msg->data.set_compute.n = comp.n;
    comp.set = true;
    comp.next_comp = true;
  } else {
    fprintf(stderr, "WARN: New computation parameters requested but it is "
                    "discarded due to on ongoing computation\n");
  }
  return ret;
}

bool compute(message *msg) {
  enable_comp();
  if (!is_computing()) {
    comp.cid = 0;
    comp.computing = true;
    comp.cur_x = comp.cur_y = 0;
    comp.next_comp = false;
    comp.chunk_re = comp.range_re_min;
    comp.chunk_im = comp.range_im_max;

    msg->type = MSG_COMPUTE;

  } else {
    fprintf(stderr, "INFO: New computation chunk id: %d for part %d x %d\n",
            msg->data.compute.cid, msg->data.compute.n_re,
            msg->data.compute.n_im);
    if (comp.cid < comp.nbr_chunks) {
      comp.cur_x += comp.chunk_n_re;
      comp.chunk_re += comp.chunk_n_re * comp.d_re;
      if (comp.cur_x >= comp.grid_w) {
        comp.cur_x = 0;
        comp.chunk_re = comp.range_re_min;
        comp.chunk_im += comp.chunk_n_im * comp.d_im;
        comp.cur_y += comp.chunk_n_im;
      }
      msg->type = MSG_COMPUTE;
    } else if (comp.cid == comp.nbr_chunks) {
      comp.done = true;
      comp.next_comp = true;
    } else {
      comp.set = false;
    }
  }
  fprintf(stderr, "INFO: Prepare new chunk of data cid: %d\n", ++comp.cid);

  if (comp.computing && msg->type == MSG_COMPUTE) {
    msg->data.compute.cid = comp.cid;
    msg->data.compute.re = comp.chunk_re;
    msg->data.compute.im = comp.chunk_im;
    msg->data.compute.n_re = comp.chunk_n_re;
    msg->data.compute.n_im = comp.chunk_n_im;
  }
  return is_computing();
}

void update_data(msg_compute_data *compute_data) {
  my_assert(compute_data != NULL, __func__, __LINE__, __FILE__);
  if (!is_abort()) {
    if (compute_data->cid == comp.cid) {
      int idx = (comp.cur_x + compute_data->i_re) +
                (comp.cur_y + compute_data->i_im) * comp.grid_w;
      if (idx >= 0 && idx < (comp.grid_h * comp.grid_w)) {
        comp.grid[idx] = compute_data->iter;
      }
      if ((comp.cid + 1) >= comp.nbr_chunks &&
          (compute_data->i_re + 1) == comp.chunk_n_re &&
          (compute_data->i_im + 1) == comp.chunk_n_im) {
      }
      // comp.done = true;
    }
  }
}

void clear_buffer() {
  info("Clear buffer");
  int chunk_num = !comp.computing ? comp.nbr_chunks : comp.cid;
  int cx = 0;
  int cy = 0;
  for (int i = 0; i < chunk_num; ++i) {
    for (int y = cy; y <= comp.chunk_n_im + cy; ++y) {
      for (int x = cx; x <= comp.chunk_n_re + cx; ++x) {
        int idx = y * comp.grid_w + x;
        comp.grid[idx] = 0;
      }
    }
    cx += comp.chunk_n_re;
    if (cx >= comp.grid_w) {
      cx = 0;
      cy += comp.chunk_n_im;
    }
  }
  gui_refresh();
}

void get_grid_size(int *w, int *h) {
  *w = comp.grid_w;
  *h = comp.grid_h;
}

void update_image(int w, int h, uint8_t *img) {
  my_assert(img && comp.grid && w == comp.grid_w && h == comp.grid_h, __func__,
            __LINE__, __FILE__);
  for (int i = 0; i < w * h; ++i) {
    double t = 1. * comp.grid[i] / (comp.n + 0.0);
    *(img++) = 9 * (1 - t) * t * t * t * 255;
    *(img++) = 15 * (1 - t) * (1 - t) * t * t * 255;
    *(img++) = 8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255;
  }
}

void compute_julia_set() {
  int n, i;
  // enable_comp();
  comp.computing = true;
  comp.done = false;

  /* for real axis */
  for (int y = 0; y < comp.grid_h; ++y) {
    for (int x = 0; x < comp.grid_w; ++x) {
      /* compute iteration */
      n = is_in_julia_set(comp, comp.range_re_min + (x * comp.d_re),
                          comp.range_im_max + (y * comp.d_im), 1);
      i = y * comp.grid_w + x;
      comp.grid[i] = n;
    }
  }
  gui_refresh();
  comp.computing = false;
  comp.done = true;
}

int is_in_julia_set(Computation comp, double z_re, double z_im, int iter) {
  double t_re = (z_re * z_re - z_im * z_im) + comp.c_re;
  double t_im = (2 * z_re * z_im) + comp.c_im;
  double t_abs = sqrt((t_re * t_re) + (t_im * t_im));
  if (t_abs < 2.0 && iter < comp.n) {
    return is_in_julia_set(comp, t_re, t_im, ++iter);
  } else {
    return iter;
  }
}

/*
void change_axes(float inc) {
  comp.range_re_max -= inc;
  comp.range_re_min += inc;
  comp.range_im_max -= inc;
  comp.range_im_min += inc;
}
*/
