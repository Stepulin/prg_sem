#include "gui.h"
#include "computation.h"
#include "utils.h"
#include "xwin_sdl.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdint.h>
#include <stdlib.h>

static struct {
  int w;
  int h;
  uint8_t *img;
} gui = {.img = NULL};

void gui_init(void) {
  get_grid_size(&gui.w, &gui.h);
  gui.img = my_alloc(gui.w * gui.h * 3);
  if (xwin_init(gui.w, gui.h) != 0) {
    exit(EXIT_FAILURE);
  }
}

void gui_cleanup(void) {
  my_assert(gui.img != NULL, __func__, __LINE__, __FILE__);
  free(gui.img);
  gui.img = NULL;
  xwin_close();
}

void gui_refresh(void) {
  my_assert(gui.img != NULL, __func__, __LINE__, __FILE__);
  info("REDRAW");
  update_image(gui.w, gui.h, gui.img);
  xwin_redraw(gui.w, gui.h, gui.img);
}

void gui_export(void) { xwin_export(); }

/*
void gui_zoom(double inc) {
  change_axes(inc);
  gui_refresh();
}
*/
