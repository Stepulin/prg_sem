#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdbool.h>
#include <stdlib.h>

void my_assert(bool r, const char *fcname, int line, const char *fname);
void *my_alloc(size_t size);
void call_termios(int reset);

void info(const char* s);
void debug(const char* s);
void error(const char* s);

#endif
