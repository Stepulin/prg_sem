#ifndef __GUI_H__
#define __GUI_H__
#include <stdint.h>
#define ZOOM_INCREMENT 0.1

void gui_init(void);
void gui_cleanup(void);
void gui_refresh(void);
void gui_export(void);
//void gui_zoom(double inc);

#endif
